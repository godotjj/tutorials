class_name SprintComponent
extends Node

@onready var _recovery_delay_timer: Timer = $RecoveryDelayTimer
@onready var _sprint_bar: ProgressBar = $CanvasLayer/SprintBar

@export_category("Required")
@export var _movement_component: MovementComponent

@export_category("Stats")
@export var _sprint_speed := 0.75
@export var _max_sprint := 100
@export var _consumption_speed := 10.0
@export var _recovery_speed := 5.0
var _recovery_speed_multiplier:
	get:
		return 1.0 if _movement_component.is_moving else 1.75

var _is_sprinting := false
var _is_recovering := false


func _ready() -> void:
	_sprint_bar.max_value = _max_sprint
	_sprint_bar.value = _sprint_bar.max_value


func _process(delta: float) -> void:
	var is_moving = _movement_component.is_moving
	var has_sprint_left = _has_sprint_left()

	_control_bar_visibility()

	if _is_recovering:
		_sprint_bar.value += _recovery_speed * _recovery_speed_multiplier * delta

	if Input.is_action_pressed("sprint") and has_sprint_left and is_moving:
		_start_sprint()
	elif (Input.is_action_just_released("sprint") or not has_sprint_left or not is_moving) and _is_sprinting:
		_stop_sprint()

	if _is_sprinting:
		_sprint(delta)


func _start_sprint():
	if _is_sprinting:
		return

	_is_sprinting = true
	_is_recovering = false
	_movement_component.speed_multiplier += _sprint_speed


func _sprint(delta: float):
	_sprint_bar.value -= _consumption_speed * delta


func _stop_sprint():
	if not _is_sprinting:
		return

	_is_sprinting = false
	_movement_component.speed_multiplier -= _sprint_speed

	_recovery_delay_timer.start()


func _control_bar_visibility():
	_sprint_bar.visible = _sprint_bar.value < _sprint_bar.max_value


func _has_sprint_left():
	return _sprint_bar.value > _sprint_bar.min_value


func _on_recovery_delay_timer_timeout() -> void:
	_is_recovering = true
