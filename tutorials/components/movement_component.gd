class_name MovementComponent
extends Node

@export_category('Required')
@export var _entity: Node
@export var _direction_input: ExternalInput

@export_category("Stats")
@export var _base_speed := 100.0
@export var speed_multiplier := 1.0

var is_moving := false


func _process(delta):
	if _direction_input.direction.length() > 0:
		is_moving = true
		_move(delta)
	else:
		is_moving = false


func _move(delta) -> void:
	_entity.position += _direction_input.direction * _base_speed * speed_multiplier * delta
