class_name PlayerInput
extends ExternalInput


func _process(_delta):
	_get_direction_from_input()


func _get_direction_from_input() -> void:
	direction = Input.get_vector("move_left", "move_right", "move_up", "move_down")

	if direction.length() > 0:
		direction = direction.normalized()
