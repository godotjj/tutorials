class_name AiInput
extends ExternalInput


func _get_random_direction() -> void:
	direction = Vector2.ZERO

	direction.x = randi_range(-1, 1)
	direction.y = randi_range(-1, 1)

	if direction.length() > 0:
		direction = direction.normalized()


func _on_timer_timeout() -> void:
	_get_random_direction()
